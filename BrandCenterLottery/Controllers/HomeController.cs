﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BrandCenterLottery.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;

            ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
            ViewData["Runtime"] = isMono ? "Mono" : ".NET";

            return View();
        }

        public ActionResult Result()
        {
            return View();
        }

        public ActionResult Done()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Lottery()
        {
            //貨號
            string keyNum_preorder = "preorder-" + Request.Form["KeyNum_preorder"].ToString().Trim();
            string keyNum_order = "order-" + Request.Form["KeyNum_order"].ToString().Trim();
            string keyNum = string.Empty;

            if (keyNum_preorder != "preorder-")
            {
                keyNum = keyNum_preorder;
            }
            else if (keyNum_order != "order-")
            {
                keyNum = keyNum_order;
            }

            if (string.IsNullOrEmpty(keyNum))
            {
                return View("/Views/Home/Infomation.cshtml");
            }

            Random Rnd = new Random(); //加入Random，產生的數字不會重覆

            int RndNum = Rnd.Next(0, 13) + 1;



            /*
             取資料      
             USE BrandCenter

             

             SELECT @Num = P.Number , @Name = P.ProductName FROM Product P Where P.Remain_Count > 0 and ( P.Number = Round(RAND() * 13,0) + 1 )

             select @Num , @Name

             UPDATE Product SET Remain_Count = (Remain_Count -1) Where Number = @Num 
             */

            if (keyNum == "preorder-12140" || keyNum == "order-12140")
            {
                return View("/Views/Home/Done.cshtml");
            }
            else
            {
                ViewData["ImagePath"] = "/Images/" + RndNum + ".jpg";
                ViewData["ProductName"] = "No." + RndNum;

                return View("/Views/Home/Result.cshtml");
            }
        }
    }
}
